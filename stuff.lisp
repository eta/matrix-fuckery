;; (ql:quickload '(cffi cl-sodium hunchentoot drakma qbase64 cl-json ironclad easy-routes cl-redis cl-conspack log4cl) :silent t)

;; stuff to do:
;; - fix RESOLVE-HTTP-DESTINATION to actually work
;; - have some sort of generic event handler thing
;; - make PDU sending not explode threads

(defpackage :matrix-fuckery
  (:use :cl))

(in-package :matrix-fuckery)

(defparameter +canonicalizer-url+ "http://127.0.0.1:5000/canon")
(defparameter *server-name* "matrix-fuckery.eta.st")
(defparameter *current-keypair* nil)

(defmacro cassoc (key alist)
  "Macro for (CDR (ASSOC KEY ALIST))."
  `(cdr (assoc ,key ,alist)))

(define-condition aval-error (error)
  ((key
    :initarg :key
    :reader key)
   (alist
    :initarg :alist
    :reader alist))
  (:report
   (lambda (ave stream)
     (format stream "failed to find ~A in ~A"
             (key ave) (alist ave)))))

(defmacro aval (key alist)
  "Retrieves the value associated with KEY from the association list ALIST, throwing an error if the value wasn't found."
  (let ((val-sym (gensym))
        (key-sym (gensym)))
    `(let* ((,key-sym ,key)
            (,val-sym (cassoc ,key-sym ,alist)))
       (unless ,val-sym
         (error 'aval-error :key ,key-sym :alist ,alist))
       ,val-sym)))

(defclass nacl-keypair ()
  ((pubkey
    :initarg :pubkey
    :accessor pubkey)
   (seckey
    :initarg :seckey
    :accessor seckey)))

(conspack:defencoding nacl-keypair pubkey seckey)

(defun cpk-base64 (object)
  (qbase64:encode-bytes
   (cpk:encode object)))

(defun cpk-unbase64 (data)
  (cpk:decode (subseq (qbase64:decode-string data) 0)))

(defun generate-nacl-keypair ()
  (let ((pk (cffi:make-shareable-byte-vector
             cr:+crypto-sign-ed-25519-publickeybytes+))
        (sk (cffi:make-shareable-byte-vector
             cr:+crypto-sign-ed-25519-secretkeybytes+)))
    (cffi:with-pointer-to-vector-data (pk-ptr pk)
      (cffi:with-pointer-to-vector-data (sk-ptr sk)
        (cr:crypto-sign-keypair pk-ptr sk-ptr)))
    (make-instance 'nacl-keypair
                   :pubkey pk
                   :seckey sk)))

(defun initialize-keypair ()
  "If a keypair already exists in Redis, retrieves that keypair and updates *CURRENT-KEYPAIR*. Otherwise, generates a new one, stores that in Redis, and does the same."
  (setf *current-keypair*
        (if (red:get "sys:keypair")
            (cpk-unbase64 (red:get "sys:keypair"))
            (let ((keypair (generate-nacl-keypair)))
              (log:info "Generated new keypair")
              (red:set "sys:keypair" (cpk-base64 keypair))
              keypair))))

(defun nacl-sign (vector)
  (let ((sm (cffi:make-shareable-byte-vector
             (+ (length vector)
                cr:+crypto-sign-ed-25519-bytes+)))
        (m-new (cffi:make-shareable-byte-vector
                (length vector)))
        (smlen (cffi:foreign-alloc :unsigned-long-long)))
    (loop
      for byte across vector
      for i from 0
      do (setf (aref m-new i) (aref vector i)))
    (cffi:with-pointer-to-vector-data (sm-ptr sm)
      (cffi:with-pointer-to-vector-data (m-ptr m-new)
        (cffi:with-pointer-to-vector-data (seckey-ptr (seckey *current-keypair*))
          (cr:crypto-sign sm-ptr smlen m-ptr (length vector) seckey-ptr))))
    (subseq
     (subseq sm 0 (cffi:mem-ref smlen :unsigned-long-long))
     0
     cr:+crypto-sign-ed-25519-bytes+)))

(defun fucked-base64 (vector)
  (delete #\= (qbase64:encode-bytes vector)))

(defun fucked-base64-uri-safe (vector)
  (delete #\= (qbase64:encode-bytes vector :scheme :uri)))

(defun canonicalize-externally (json)
  (drakma:http-request +canonicalizer-url+
                       :method :post
                       :content json
                       :content-type "application/json"))

(defun sign-and-jsonify (data &key (transform-by #'identity))
  (let* ((data-without-unsigned
           (remove-if
            (lambda (thing)
              (or
               (eql (car thing) :unsigned)
               (eql (car thing) :signatures)))
            data))
         (transformed (funcall transform-by data-without-unsigned))
         (unsigned (assoc :unsigned data))
         (fixed-signatures
           (mapcar
            (lambda (sig-elt)
              (cons
               (string-downcase
                (symbol-name (car sig-elt)))
               (cdr sig-elt)))
            (cdr (assoc :signatures data))))
         (uncanonicalized
           (cl-json:encode-json-to-string transformed))
         (canonicalized
           (canonicalize-externally uncanonicalized))
         (signed (nacl-sign canonicalized))
         (signed-b64 (fucked-base64 signed)))
    (append `((:signatures . ((,*server-name* . (("ed25519:meow" . ,signed-b64)))
                              ,@fixed-signatures))
              ,@(when unsigned (list unsigned)))
            data-without-unsigned)))

;; NOTE leaves out content on purpose
(defparameter +redaction-allowed-keys+
  '(:event--id :type :room--id :sender :state--key :hashes
    :signatures :depth :prev--events :prev--state :auth--events :origin
    :origin--server--ts :membership))
(defparameter +redaction-allowed-content-keys+
  '(("m.room.member" . (:membership))
    ("m.room.create" . (:creator))
    ("m.room.join_rules" . (:join--rule))
    ("m.room.power_levels" . (:ban :events :events--default :kick :redact
                                   :state--default :users :users--default))
    ("m.room.aliases" . (:aliases))
    ("m.room.history_visibility" . (:history--visibility))))

(defun if-nil-objectify (val)
  "If VAL is NIL, return an empty hash table; else, return VAL."
  (if val
      val
      (make-hash-table)))

(defun if-nil-listify (val)
  "If VAL is NIL, return an empty vector; else, return VAL."
  (if val
      val
      #()))

(defun redact-matrix-event (event)
  "Perform the Matrix event redaction algorithm on EVENT."
  (let ((allowed-content-keys
          (cdr (assoc (aval :type event)
                      +redaction-allowed-content-keys+
                      :test #'equal)))
        (content (aval :content event)))
    ;; FIXME FIXME pretty hacky lol
    (when (assoc :prev--state event)
      (unless (cassoc :prev--state event)
        (setf (cassoc :prev--state event) #())))
    `((:content
       .
       ,(if-nil-objectify ; lol hacks
         (remove-if-not
          (lambda (content-elt)
            (member (car content-elt) allowed-content-keys))
          content)))
      ,@(remove-if-not
         (lambda (event-elt)
           (member (car event-elt) +redaction-allowed-keys+))
         event))))

(defun sign-matrix-event (event)
  (sign-and-jsonify event
                    :transform-by #'redact-matrix-event))

(defun get-v4-event-id (event)
  (let* ((redacted (redact-matrix-event event))
         (without-data
             (remove-if
              (lambda (ent)
                (member (car ent) '(:signatures :age--ts :unsigned)))
              redacted))
         (canonicalized
           (canonicalize-externally
            (cl-json:encode-json-to-string
             without-data))))
    (format nil "$~A"
            (fucked-base64-uri-safe
             (ironclad:digest-sequence :sha256 canonicalized)))))

(defun hash-matrix-event (event)
  "Calculates the content hash for the Matrix EVENT."
  (let* ((without-data
             (remove-if
              (lambda (ent)
                (member (car ent) '(:unsigned :signatures :hashes)))
              event))
         (canonicalized
           (canonicalize-externally
            (cl-json:encode-json-to-string
             without-data))))
    (format nil "~A"
            (fucked-base64
             (ironclad:digest-sequence :sha256 canonicalized)))))

(defun enhashen-matrix-event (event)
  "Calculates the content hash for the Matrix EVENT, and adds it to the event."
  `((:hashes
     .
     ((:sha256
       .
       ,(hash-matrix-event event))))
    ,@event))

(defun generate-keys-payload ()
  `((:old_verify_keys . ,(make-hash-table))
    (:server_name . ,*server-name*)
    (:valid_until_ts . 1653861112000) ; one year after this was written
    (:verify_keys
     .
     (("ed25519:meow"
       .
       ((:key
         .
         ,(fucked-base64 (pubkey *current-keypair*)))))))))

(define-condition matrix-error (error)
  ((errcode
    :initarg :errcode
    :reader errcode)
   (error-text
    :initarg :error-text
    :reader error-text)
   (status-code
    :initarg :status-code
    :reader status-code)
   (full-error
    :initarg :full-error
    :reader full-error))
  (:report
   (lambda (mxe stream)
     (format stream "Matrix error (~A / ~A): ~A~%raw: ~A"
             (errcode mxe) (status-code mxe) (error-text mxe)
             (full-error mxe)))))

(defun resolve-http-destination (dest-hs)
  dest-hs)

(defun federation-request (method uri target-hs data)
  (let* ((target-http (resolve-http-destination target-hs))
         (target-url (format nil "https://~A/_matrix/~A" target-http uri))
         (body (sign-and-jsonify
                `((:method . ,(symbol-name method))
                  (:uri . ,(format nil "/_matrix/~A" uri))
                  (:origin . ,*server-name*)
                  (:destination . ,target-hs)
                  ,@(when data
                      (list (cons :content data))))))
         (auth-header
           (format nil "X-Matrix origin=~A,key=\"~A\",sig=\"~A\""
                   *server-name*
                   "ed25519:meow"
                   (cdadr (cadr (assoc :signatures body))))))
    (multiple-value-bind (out-data status-code)
        (drakma:http-request target-url
                             :method method
                             :preserve-uri t
                             :content (cl-json:encode-json-to-string
                                       data)
                             :external-format-in :utf-8
                             :external-format-out :utf-8
                             :content-type "application/json"
                             :additional-headers `(("Authorization" . ,auth-header)))
      (let ((json (cl-json:decode-json-from-string
                   (babel:octets-to-string
                    out-data
                    :encoding :utf-8))))
        (unless (eql status-code 200)
          (error 'matrix-error
                 :status-code status-code
                 :full-error
                 (remove-if
                  (lambda (ent)
                    (member (car ent) '(:error :errcode)))
                  json)
                 :error-text (cassoc :error json)
                 :errcode (cassoc :errcode json)))
        json))))

(defun store-state-event (state-event)
  (let* ((room-id (aval :room--id state-event))
         (evt-type (aval :type state-event))
         (state-key (aval :state--key state-event))
         (event-id (get-v4-event-id state-event))
         (redis-name (format nil "event:~A" event-id)))
    (log:info "storing ~A (sk ~A id ~A) state event for room ~A"
              evt-type state-key event-id room-id)
    (red:set redis-name (cpk-base64 state-event))
    (red:set (format nil "room:~A:state:~A:~A" room-id evt-type state-key)
             redis-name)
    (red:sadd (format nil "room:~A:new-state-list" room-id) redis-name)))

(defun process-room-state-list (state-blob)
  (let* ((room-id (aval :room--id (first state-blob)))
         (nsl (format nil "room:~A:new-state-list" room-id))
         (sl (format nil "room:~A:state-list" room-id)))
    (log:info "processing ~A state events for room ~A"
              (length state-blob)
              (aval :room--id (first state-blob)))
    (dolist (state-evt state-blob)
      (store-state-event state-evt))
    (when (red:exists sl)
      (log:warn "clobbering ~A old state events for room ~A"
                (red:scard sl) room-id)
      (red:del sl))
    (red:rename nsl sl)))

(defun get-fresh-state (room-id server)
  (log:info "asking ~A to resolve state for room ~A"
            server room-id)
  ;; mwahaha
  (let* ((data
           (federation-request
            :get
            (format nil "federation/v1/state/~A"
                    (hunchentoot:url-encode room-id))
            server
            nil))
         (new-state (aval :pdus data)))
    (process-room-state-list new-state)
    (red:del (format nil "room:~A:new-state-list" room-id))))

(defun process-edu (event)
  (let* ((edu-type (aval :edu--type event))
         (content (aval :content event)))
    (log:info "got EDU of type ~A: ~A" edu-type content)))

(defun process-pdu (event)
  (let* ((room-id (aval :room--id event))
         (evt-type (aval :type event))
         (sender (aval :sender event))
         (state-key (cassoc :state--key event))
         (event-id (get-v4-event-id event))
         (redis-name (format nil "event:~A" event-id)))
    (log:info "processing PDU of type ~A (id ~A) from ~A for room ~A"
              evt-type event-id sender room-id)
    (red:set redis-name (cpk-base64 event))
    (if state-key
        (progn
          (log:warn "PDU ~A was a state event, state must now be recomputed" event-id)
          (red:sadd (format nil "room:~A:new-state-list" room-id) redis-name))
        (progn
          (red:sadd (format nil "room:~A:event-list" room-id) redis-name)))))

(defun get-fresh-state-if-needed (room-id)
  (when (red:exists (format nil "room:~A:new-state-list" room-id))
    (log:warn "room ~A requires state resolution" room-id)
    (let* ((member-servers (room-member-servers room-id))
           (random-server
             (nth (random (length member-servers)) member-servers)))
      (log:warn "picked unwitting victim ~A from ~A possibilities"
                random-server (length member-servers))
      (get-fresh-state room-id random-server))))

(defun get-all (pattern)
  "Calls RED:SCAN in a loop, returning all the Redis keys matching PATTERN."
  (loop
    with cursor
    with cur
    while (or (not cursor) (not (string= cursor "0")))
    do (setf cur (red:scan (or cursor "0") :match pattern :count 1000))
    do (setf cursor (first cur))
    append (second cur)))

(defun red-get-unbase64 (key)
  (let ((data (red:get key)))
    (when data
      (cpk-unbase64 data))))

(defun room-state (room-id type &optional state-key)
  "Get the room state event of type TYPE for ROOM-ID, with an optional STATE-KEY."
  (red-get-unbase64
   (red:get
    (format nil "room:~A:state:~A:~A"
            room-id type (or state-key "")))))

(defun room-state-all (room-id type)
  "Get all room state events of type TYPE for ROOM-ID."
  (let* ((keys (get-all (format nil "room:~A:state:~A:*"
                                room-id type))))
    (mapcar
     (lambda (x) (red-get-unbase64 (red:get x)))
     keys)))

(defun find-auth-events (room-id sender)
  (log:info "finding auth events for ~A to send something in ~A"
            sender room-id)
  (let ((creation (room-state room-id "m.room.create"))
        (power-levels (room-state room-id "m.room.power_levels"))
        (member (room-state room-id "m.room.member" sender)))
    (remove-if #'null (list creation power-levels member))))

(defun find-auth-event-ids (room-id sender)
  (mapcar
   #'get-v4-event-id
   (find-auth-events room-id sender)))

(defun find-forward-extremities (room-id)
  (log:info "calculating forward extremities for ~A (~A evts)"
            room-id (red:scard (format nil "room:~A:event-list" room-id)))
  (let ((events
          (mapcar #'red-get-unbase64 (red:smembers
                                      (format nil "room:~A:event-list" room-id))))
        (prev-event-list '()))
    (dolist (evt events)
      (dolist (prev (aval :prev--events evt))
        (pushnew prev prev-event-list :test #'string=)))
    (remove-if
     (lambda (evt)
       (member (get-v4-event-id evt) prev-event-list
               :test #'string=))
     events)))

(defparameter +js-int-max+ (1- (expt 2 63)))

(defun get-max-depth (event-list)
  (let ((ret
          (loop
            for evt in event-list
            maximizing (aval :depth evt))))
    (min +js-int-max+ ret)))

(defun find-forward-extremity-ids (room-id)
  (mapcar
   #'get-v4-event-id
   (find-forward-extremities room-id)))

(defun get-mxid-server (mxid)
  (let ((pos (position #\: mxid)))
    (unless pos
      (error "bad mxid: ~A" mxid))
    (subseq mxid (1+ pos))))

(defun room-members (room-id)
  (mapcar
   (lambda (pdu) (aval :state--key pdu))
   (room-state-all room-id "m.room.member")))

(defun room-member-servers (room-id &key include-self)
  (remove-if
   (lambda (srv)
     (and (not include-self) (string= srv *server-name*)))
   (remove-duplicates
    (mapcar
     #'get-mxid-server
     (room-members room-id))
    :test #'string=)))

(defparameter *unix-epoch-difference*
  (encode-universal-time 0 0 0 1 1 1970 0))

(defun universal-to-unix-time (universal-time)
  (- universal-time *unix-epoch-difference*))

(defun unix-to-universal-time (unix-time)
  (+ unix-time *unix-epoch-difference*))

(defun get-unix-time ()
  (universal-to-unix-time (get-universal-time)))

(defun get-unix-time-ms ()
  (* 1000 (get-unix-time)))

(defun make-pdu (room-id mxid type content)
  (get-fresh-state-if-needed room-id)
  (let* ((auth-events (find-auth-event-ids room-id mxid))
         (prev-event-list (find-forward-extremities room-id))
         (prev-events (mapcar #'get-v4-event-id prev-event-list))
         (depth (get-max-depth prev-event-list)))
    (sign-matrix-event
     (enhashen-matrix-event
      `((:auth--events . ,auth-events)
        (:prev--events . ,prev-events)
        (:depth . ,depth)
        (:sender . ,mxid)
        (:origin . ,*server-name*)
        (:content . ,content)
        (:type . ,type)
        (:origin--server--ts . ,(get-unix-time-ms))
        (:room--id . ,room-id))))))

(defun make-simple-text-pdu (room-id mxid text)
  (make-pdu room-id mxid "m.room.message"
            `((:body . ,text)
              (:msgtype . "m.text"))))

(defun send-pdu (remote-server pdu &key txid)
  (let ((txid (or txid (random +js-int-max+)))
        (event-id (get-v4-event-id pdu)))
    (log:info "sending PDU ~A to ~A" event-id remote-server)
    (let* ((result
             (federation-request
              :put
              (format nil "federation/v1/send/~A"
                      txid)
              remote-server
              `((:origin . ,*server-name*)
                (:origin--server--ts . ,(get-unix-time-ms))
                (:pdus . ,(list pdu)))))
           (pdu-key
             (intern
              (cl-json:lisp-to-camel-case event-id)
              :keyword))
           (ret (cassoc pdu-key result)))
      (when ret
        (log:error "~A rejected PDU offering ~A: ~A"
                   remote-server event-id ret)
        (error "PDU rejected: ~A" (cassoc :error ret)))
      (log:info "sent PDU ~A to ~A" event-id remote-server))))

(defun async-send-pdu (remote-server pdu &key txid)
  "Spawns a thread to call SEND-PDU in, with the provided arguments, and returns it."
  (bt:make-thread
   (lambda ()
     (handler-case
         (progn
           (send-pdu remote-server pdu
                     :txid txid)
           nil)
       (error (e)
         (log:error "failed to send PDU ~A to ~A: ~A"
                    (get-v4-event-id pdu)
                    remote-server e)
         e)))
   :name (format nil "pdu sender for ~A" remote-server)))

(defun broadcast-pdu (pdu)
  (let* ((room-id (aval :room--id pdu))
         (event-id (get-v4-event-id pdu))
         (member-servers (room-member-servers room-id)))
    (log:info "broadcasting PDU ~A to ~A servers"
              event-id
              (length member-servers))
    (let* ((threads
             (mapcar
              (lambda (srv)
                (async-send-pdu srv pdu))
              member-servers))
           (errors
             (remove-if
              #'null
              (mapcar
               #'bt:join-thread
               threads))))
      (when (eql (length errors) (length member-servers))
        (error "No servers accepted the PDU. Errors: ~{~A~^; ~}"
               errors)))
    (process-pdu pdu)))

(defun join-room (room-id user-id remote-server)
  (log:info "attempting to join ~A on ~A as ~A..."
            room-id remote-server user-id)
  (let* ((make-join-url
           (format nil "federation/v1/make_join/~A/~A?ver=4&ver=5&ver=6"
                   (hunchentoot:url-encode room-id)
                   (hunchentoot:url-encode user-id)))
         (send-join-url
           (format nil "federation/v2/send_join/~A/~A"
                   (hunchentoot:url-encode room-id)
                   (hunchentoot:url-encode user-id)))
         (make-join-resp
           (federation-request :get make-join-url
                               remote-server nil))
         (event (aval :event make-join-resp))
         (room-version (aval :room--version make-join-resp)))
    (log:info "turns out room version is ~A"
              room-version)
    (red:set (format nil "room:~A:version" room-id) room-version)
    (setf (cassoc :origin event) *server-name*)
    (setf (cassoc :sha-256 (cassoc :hashes event))
          (hash-matrix-event event))
    (let ((result
            (federation-request :put send-join-url remote-server (sign-matrix-event event))))
      (process-room-state-list
       (aval :state result)))))

(defun get-profile-information (mxid)
  "Retrieves user profile information for the given MXID."
  (let ((hostname (subseq mxid (1+ (position #\: mxid))))
        (url-mxid (hunchentoot:url-encode mxid)))
    (log:info "getting profile info for ~A from ~A" url-mxid hostname)
    (federation-request
     :get
     (format nil "federation/v1/query/profile?user_id=~A"
             url-mxid)
     hostname
     nil)))

(easy-routes:defroute keys ("/_matrix/key/v2/server/:key") ()
  (log:info "asked for key ~A~%" key)
  (unless (string= key "ed25519:meow")
    (error "key ~A not sufficiently nya~~" key))
  (setf (tbnl:content-type*) "application/json")
  (cl-json:encode-json-to-string
   (sign-and-jsonify
    (generate-keys-payload))))

(defun get-user-localpart (mxid)
  (unless (and
           (eql (elt mxid 0) #\@)
           (position #\: mxid))
    (error "malformed mxid: ~A" mxid))
  (let ((server-part
          (subseq mxid (1+ (position #\: mxid)))))
    (unless (equal server-part *server-name*)
      (error "tried to get the localpart of non-local mxid ~A" mxid)))
  (subseq mxid 1 (position #\: mxid)))

(easy-routes:defroute profile ("/_matrix/federation/v1/query/profile") (user_id)
  (log:info "asked for profile ~A" user_id)
  (let* ((localpart (get-user-localpart user_id))
         (displayname (red:get (format nil "user:~A:name" localpart))))
    (unless displayname
      (setf (tbnl:return-code*) 404)
      (hunchentoot:abort-request-handler "nope"))
    (setf (tbnl:content-type*) "application/json")
    (cl-json:encode-json-to-string
     `((:displayname . "eta-test")
       (::avatar--url . nil)))))

(defparameter +matrix-origin-scanner+
  (cl-ppcre:create-scanner "X-Matrix origin=([^,]+)"))

(defun extract-origin-from-auth-header (header)
  (cl-ppcre:register-groups-bind (origin) (+matrix-origin-scanner+ header)
    origin))

(defun matrix-federation-payload ()
  "Returns, as multiple values, the name of the sending homeserver, and the decoded JSON post data."
  (let* ((origin
           (extract-origin-from-auth-header
            (hunchentoot:header-in* :authorization)))
         (body-text (hunchentoot:raw-post-data
                       :external-format :utf-8
                       :force-text t))
         (json-body (cl-json:decode-json-from-string body-text)))
    (unless origin
      (error "sending matrix homeserver did not include Authorization header"))
    (values origin json-body)))

(easy-routes:defroute invite ("/_matrix/federation/v2/invite/:room-id/:event-id" :method :put) ()
  (multiple-value-bind (origin payload) (matrix-federation-payload)
    (let* ((user-id (aval :state--key (aval :event payload)))
           (user-localpart (subseq user-id 0 (position #\: user-id))))
      (log:info "local user ~A invited to room ~A by ~A with event ~A (room version ~A)~%"
                user-id
                room-id origin event-id (aval :room--version payload))
      (red:set (format nil "user:~A:invite:~A" user-localpart room-id)
               (cpk-base64
                (list origin event-id payload)))
      (red:sadd (format nil "user:~A:invites" user-localpart) room-id)
      ;; sign the event given to us and return it
      (setf (tbnl:content-type*) "application/json")
      (cl-json:encode-json-to-string
       `((:event
          .
          ,(sign-matrix-event
            (aval :event payload))))))))

(easy-routes:defroute pdu ("/_matrix/federation/v1/send/:txn-id" :method :put) ()
  (multiple-value-bind (origin payload) (matrix-federation-payload)
    (let* ((edus (cassoc :edus payload))
           (pdus (cassoc :pdus payload))
           (asserted-origin (aval :origin payload))
           (txn-redis-name (format nil "origin-txns:~A"
                                   txn-id))
           (pdus-list '()))
      (unless (equal asserted-origin origin)
        (error "server ~A tried to be ~A in /send" origin asserted-origin))
      (if (red:sismember txn-id txn-redis-name)
          (log:warn "Skipping already processed txn ~A from ~A"
                    txn-id origin)
          (progn
            (log:info "received ~A PDUs and ~A EDUs from ~A"
                      (length pdus) (length edus) origin)
            (dolist (edu edus)
              (process-edu edu))
            (dolist (pdu pdus)
              (let ((event-id (get-v4-event-id pdu))
                    (err nil))
                (handler-case
                    (process-pdu pdu)
                  (error (e)
                    (log:error "Failed to process PDU ~A: ~A"
                               event-id e)
                    (setf err (format nil "oops: ~A" e))))
                (push (cons event-id
                            (if err
                                (list (cons :error err))
                                (make-hash-table)))
                      pdus-list)))
            (red:sadd txn-redis-name txn-id)))
      (setf (tbnl:content-type*) "application/json")
      (cl-json:encode-json-to-string
       `((:pdus . ,(if-nil-objectify pdus-list)))))))

(easy-routes:defroute version ("/_matrix/federation/v1/version") ()
  (setf (tbnl:content-type*) "application/json")
  (cl-json:encode-json-to-string
   `(:server
     .
     ((:name . "matrix-fuckery")
      (:version . "destroyer of rooms")))))

(easy-routes:defroute stop ("/stop") ()
  (hunchentoot:stop hunchentoot:*acceptor*)
  "stopped")

(defparameter *redis-port* 6379)

(defmethod hunchentoot:acceptor-dispatch-request :around (acceptor request)
  (redis:with-connection (:port *redis-port*)
    (call-next-method acceptor request)))

(defun start-webserver ()
  (hunchentoot:start (make-instance 'easy-routes:routes-acceptor :port 7777 :address "0.0.0.0")))

(defun cl-json::camel-case-to-lisp (string)
  "Take a camel-case string and convert it into a string with
Lisp-style hyphenation."
  (if (position #\. string)
      string
      (apply #'concatenate 'string
             (cl-json::camel-case-transform (cl-json::camel-case-split string)))))

(defvar *orig-lisp-to-cc* (fdefinition 'cl-json::lisp-to-camel-case))

(defun cl-json::lisp-to-camel-case (string)
  (if (position #\. string)
      string
      (funcall *orig-lisp-to-cc* string)))

(setf *random-state* (make-random-state t))
